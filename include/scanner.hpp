#ifndef SCANNER_H
#define SCANNER_H

#include <fstream>
#include <iostream>
#include <string>

#include "token.hpp"

namespace qlc {

using number = std::variant<int, float>;

class scanner {
    std::ifstream& source;
    char current;
    std::string filename;
    std::size_t lineno;
    std::size_t offset;

    char& next() {
        source.get(current);
        offset += 1;
        return current;
    }

    char peek() {
        char result{};
        source.get(result);
        source.putback(result);
        return result;
    }

    void undo() {
        source.putback(current);
        offset -= 1;
    }

    std::optional<symbol> scan_identifier() {
        if (std::isalpha(current)) {
            std::string result{ current };
            while (next()) {
                if (source.eof()) return symbol{ result };
                if (current == '_' or std::isalnum(current)) {
                    result += current;
                } else {
                    undo();
                    return symbol{ result };
                }
            }
        }
        return std::nullopt;
    }

    std::optional<number> scan_number() {
        if (std::isdigit(static_cast<unsigned char>(current))) {
            std::string result{ current };
            auto decimal = false;
            while (next()) {
                if (source.eof()) goto num_done;
                if (std::isdigit(static_cast<unsigned char>(current))) {
                    result += current;
                } else if (current == '.') {
                    if (decimal) {
                        undo();
                        goto num_done;
                    } else {
                        decimal = true;
                        result += current;
                    }
                } else {
                    undo();
                    goto num_done;
                }
            }
        num_done:
            if (decimal) {
                return std::stof(result);
            } else {
                return std::stoi(result);
            }
        }
        return std::nullopt;
    }

    std::optional<std::string> scan_string() {
        std::string result{};
        while (next()) {
            if (source.eof()) break;
            if (current == '\\') {
                result += current;
                result += next();
            } else if (current == '"') {
                return result;
            } else {
                result += current;
            }
        }
        return std::nullopt;
    }

    comment scan_comment() {
        std::string result{};
        while (next()) {
            if (source.eof()) break;
            if (current == '\n') {
                undo();
                break;
            } else
                result += current;
        }
        result.erase(result.begin(), std::find_if(result.begin(), result.end(), [](unsigned char c) {
            return !std::isspace(c);
        }));
        return comment{ result };
    }

public:
    scanner(std::ifstream& s, std::string f)
        : source(s)
        , filename(f) {
        lineno = 1;
        offset = 0;
    }
    scanner& operator>>(token&);

    operator bool() {
        static auto once = true;
        if (source.eof()) {
            if (once) {
                once = false;
                return true;
            } else {
                return false;
            }
        }
        return true;
    }
};
} // namespace qlc
#endif /* SCANNER_H */