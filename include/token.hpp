#ifndef TOKEN_H
#define TOKEN_H

#include <map>
#include <optional>
#include <sstream>
#include <string>
#include <variant>

#include <gsl/gsl>
#include <rang/rang.hpp>

// clang-format off
#define KIND_LIST        \
    t(eof)               \
    t(illegal)           \
    t(integer)           \
    t(fpoint)            \
    t(string)            \
    t(identifier)        \
    t(braceopen)         \
    t(braceclose)        \
    t(parenopen)         \
    t(parenclose)        \
    t(brackopen)         \
    t(brackclose)        \
    t(comma)             \
    t(comment)           \
    t(dash_arrow_right)  \
    t(dash_arrow_left)   \
    t(eq_arrow)          \
    t(tilde)             \
    t(tilde_tilde)       \
    t(tilde_arrow_right) \
    t(tilde_arrow_left)  \
    t(lt_eq)             \
    t(gt_eq)             \
    t(dot)               \
    t(dot_eq)            \
    t(dot_dot)           \
    t(ellipsis)          \
    t(colon)             \
    t(colon_eq)          \
    t(caret)             \
    t(caret_eq)          \
    t(eq)                \
    t(eq_eq)             \
    t(bang)              \
    t(bang_bang)         \
    t(bang_eq)           \
    t(qmark)             \
    t(qmark_qmark)       \
    t(qmark_eq)          \
    t(fslash)            \
    t(fslash_fslash)     \
    t(fslash_eq)         \
    t(bslash)            \
    t(bslash_bslash)     \
    t(octothorpe)        \
    t(at)                \
    t(tilde_at)          \
    t(lt)                \
    t(lt_lt)             \
    t(gt)                \
    t(gt_gt)             \
    t(star)              \
    t(star_star)         \
    t(plus)              \
    t(plus_plus)         \
    t(amp)               \
    t(amp_amp)           \
    t(pipe)              \
    t(pipe_pipe)         \
    t(percent)           \
    t(percent_percent)   \
    t(dash)
// clang-format on

namespace qlc {

template <typename... Ts>
struct destinations : Ts... {
    using Ts::operator()...;
};
template <typename... Ts>
destinations(Ts...)->destinations<Ts...>;

enum class token_kind {
#define t(k) k,
    KIND_LIST
#undef t
};

struct symbol {
    std::string raw;
};

struct comment {
    std::string text;
};

using data_variant
    = std::variant<int, float, std::string, symbol, comment>;
using std::optional;
class token {

    optional<token_kind> kind;
    optional<std::string::size_type> lineno;
    optional<std::string::size_type> offset;
    optional<data_variant> data;

public:
    token() {}
    token(token_kind k,
        optional<std::string::size_type> l = std::nullopt,
        optional<std::string::size_type> o = std::nullopt,
        optional<data_variant> d = std::nullopt)
        : kind(k)
        , lineno(l)
        , offset(o)
        , data(d) {}

    // Creates a string representation of a token
    std::string to_string();

    std::string::size_type length();
};

// Takes a kind enum value and converts to a string
std::string kind_string(const token_kind&);
} // namespace qlc
#endif /* TOKEN_H */