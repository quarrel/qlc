#include "scanner.hpp"

namespace qlc {

scanner& scanner::operator>>(token& tok) {
scan:
    next();
    if (source.eof()) {
        tok = token(token_kind::eof, lineno, offset);
        goto done;
    }
    switch (current) {
    case ' ':
        [[fallthrough]];
    case '\t':
        goto scan;
    case '\n':
        lineno++;
        goto scan;
    case ',':
        tok = token(token_kind::comma, lineno, offset, symbol{ "," });
        goto done;
    case ';':
        tok = token(token_kind::comment, lineno, offset, scan_comment());
        goto done;
    case '(':
        tok = token(token_kind::parenopen, lineno, offset, symbol{ "(" });
        goto done;
    case ')':
        tok = token(token_kind::parenclose, lineno, offset, symbol{ ")" });
        goto done;
    case '[':
        tok = token(token_kind::brackopen, lineno, offset, symbol{ "[" });
        goto done;
    case ']':
        tok = token(token_kind::brackclose, lineno, offset, symbol{ "]" });
        goto done;
    case '{':
        tok = token(token_kind::braceopen, lineno, offset, symbol{ "{" });
        goto done;
    case '}':
        tok = token(token_kind::braceclose, lineno, offset, symbol{ "}" });
        goto done;
    case '<':
        next();
        switch (current) {
        case '-':
            tok = token(token_kind::dash_arrow_left, lineno, offset, symbol{ "<-" });
            goto done;
        case '=':
            tok = token(token_kind::lt_eq, lineno, offset, symbol{ "<=" });
            goto done;
        case '~':
            tok = token(token_kind::tilde_arrow_left, lineno, offset, symbol{ "<~" });
            goto done;
        case '<':
            tok = token(token_kind::lt_lt, lineno, offset, symbol{ "<<" });
            goto done;
        default:
            undo();
            tok = token(token_kind::lt, lineno, offset, symbol{ "<" });
            goto done;
        }
    case '>':
        next();
        switch (current) {
        case '>':
            tok = token(token_kind::gt_eq, lineno, offset, symbol{ ">>" });
            goto done;
        case '=':
            tok = token(token_kind::gt_eq, lineno, offset, symbol{ ">=" });
            goto done;
        default:
            tok = token(token_kind::gt, lineno, offset, symbol{ ">" });
            goto done;
        }
    case '-':
        next();
        switch (current) {
        case '>':
            tok = token(token_kind::dash_arrow_right, lineno, offset, symbol{ "->" });
            goto done;
        default:
            undo();
            tok = token(token_kind::dash, lineno, offset, symbol{ "-" });
            goto done;
        }
    case '=':
        next();
        switch (current) {
        case '>':
            tok = token(token_kind::eq_arrow, lineno, offset, symbol{ "=>" });
            goto done;
        case '=':
            tok = token(token_kind::eq_eq, lineno, offset, symbol{ "==" });
            goto done;
        default:
            undo();
            tok = token(token_kind::eq, lineno, offset, symbol{ "=" });
            goto done;
        }
    case '+':
        next();
        switch (current) {
        case '+':
            tok = token(token_kind::plus_plus, lineno, offset, symbol{ "++" });
            goto done;
        default:
            undo();
            tok = token(token_kind::plus, lineno, offset, symbol{ "+" });
            goto done;
        }
    case '/':
        next();
        switch (current) {
        case '/':
            tok = token(token_kind::fslash_fslash, lineno, offset, symbol{ "//" });
            goto done;
        case '=':
            tok = token(token_kind::fslash_eq, lineno, offset, symbol{ "/=" });
            goto done;
        default:
            undo();
            tok = token(token_kind::fslash, lineno, offset, symbol{ "/" });
            goto done;
        }
    case '*':
        next();
        switch (current) {
        case '*':
            tok = token(token_kind::star_star, lineno, offset, symbol{ "**" });
            goto done;
        default:
            undo();
            tok = token(token_kind::star, lineno, offset, symbol{ "*" });
            goto done;
        }
    case '.':
        next();
        switch (current) {
        case '=':
            tok = token(token_kind::dot_eq, lineno, offset, symbol{ ".=" });
            goto done;
        case '.':
            next();
            switch (current) {
            case '.':
                tok = token(token_kind::ellipsis, lineno, offset, symbol{ "..." });
                goto done;
            default:
                undo();
                tok = token(token_kind::dot_dot, lineno, offset, symbol{ ".." });
                goto done;
            }
        default:
            undo();
            tok = token(token_kind::dot, lineno, offset, symbol{ "." });
            goto done;
        }
    case ':':
        next();
        switch (current) {
        case '=':
            tok = token(token_kind::colon_eq, lineno, offset, symbol{ ":=" });
            goto done;
        default:
            undo();
            tok = token(token_kind::colon, lineno, offset, symbol{ ":" });
            goto done;
        }
    case '^':
        next();
        switch (current) {
        case '=':
            tok = token(token_kind::caret_eq, lineno, offset, symbol{ "^=" });
            goto done;
        default:
            undo();
            tok = token(token_kind::caret, lineno, offset, symbol{ "^" });
            goto done;
        }
    case '~':
        next();
        switch (current) {
        case '~':
            tok = token(token_kind::tilde_tilde, lineno, offset, symbol{ "~~" });
            goto done;
        case '>':
            tok = token(token_kind::tilde_arrow_right, lineno, offset, symbol{ "~>" });
            goto done;
        case '@':
            tok = token(token_kind::tilde_at, lineno, offset, symbol{ "~>" });
            goto done;
        default:
            undo();
            tok = token(token_kind::tilde, lineno, offset, symbol{ "~" });
            goto done;
        }
    case '!':
        next();
        switch (current) {
        case '!':
            tok = token(token_kind::bang_bang, lineno, offset, symbol{ "!!" });
            goto done;
        case '=':
            tok = token(token_kind::bang_eq, lineno, offset, symbol{ "!=" });
            goto done;
        default:
            undo();
            tok = token(token_kind::bang, lineno, offset, symbol{ "!" });
            goto done;
        }
    case '?':
        next();
        switch (current) {
        case '?':
            tok = token(token_kind::qmark_qmark, lineno, offset, symbol{ "??" });
            goto done;
        case '=':
            tok = token(token_kind::qmark_eq, lineno, offset, symbol{ "?=" });
            goto done;
        default:
            undo();
            tok = token(token_kind::qmark, lineno, offset, symbol{ "?" });
            goto done;
        }
    case '&':
        next();
        switch (current) {
        case '&':
            tok = token(token_kind::amp_amp, lineno, offset, symbol{ "&&" });
            goto done;
        default:
            undo();
            tok = token(token_kind::amp, lineno, offset, symbol{ "&" });
            goto done;
        }
    case '|':
        next();
        switch (current) {
        case '|':
            tok = token(token_kind::pipe_pipe, lineno, offset, symbol{ "||" });
            goto done;
        default:
            undo();
            tok = token(token_kind::pipe, lineno, offset, symbol{ "||" });
            goto done;
        }
    case '%':
        next();
        switch (current) {
        case '%':
            tok = token(token_kind::percent_percent, lineno, offset, symbol{ "%%" });
            goto done;
        default:
            undo();
            tok = token(token_kind::percent, lineno, offset, symbol{ "%" });
            goto done;
        }
    case '#':
        tok = token(token_kind::octothorpe, lineno, offset, symbol{ "#" });
        goto done;
    case '@':
        tok = token(token_kind::at, lineno, offset, symbol{ "@" });
        goto done;
    case '"':
        if (std::optional<std::string> s = scan_string(); s.has_value()) {
            tok = token(token_kind::string, lineno, offset, s);
            goto done;
        } else {
            tok = token(token_kind::illegal, lineno, offset);
            goto done;
        }
    default:
        if (std::optional<symbol> ident = scan_identifier(); ident.has_value())
            tok = token(token_kind::identifier, lineno, offset, ident.value());
        else if (std::optional<number> num = scan_number(); num.has_value())
            std::visit(destinations{ // clang-format off
                [&](int& i) { tok = token(token_kind::integer, lineno, offset, i); },
                [&](float& f) { tok = token(token_kind::fpoint, lineno, offset, f); }
            }, num.value()); // clang-format on
        else
            tok = token(token_kind::illegal, lineno, offset);
    }
done:
    return *this;
}
} // namespace qlc