#include "token.hpp"

namespace qlc {

std::string token::to_string() {
    rang::setControlMode(rang::control::Force);
    std::stringstream result{};
    result << "{" + qlc::kind_string(kind.value());
    if (data.has_value()) // clang-format off
        std::visit(destinations{
            [&result](int& dig) {
                result << " " << rang::fg::cyan << std::to_string(dig);
            },
            [&result](float& flt) {
                result << " " << rang::fg::cyan << std::to_string(flt);
            },
            [&result](std::string& str) {
                result << " " << rang::fg::green << "\"" + str + "\"";
            },
            [&result](symbol sym) {
                result << " " << rang::fg::yellow << "`" + sym.raw + "`";
            },
            [&result](comment cmt) {
                result << " " << rang::style::dim << "\"" + cmt.text + "\"";
            }
        }, data.value());
    result << rang::fg::reset << rang::style::reset << "}";
    rang::setControlMode(rang::control::Off);
    return result.str(); // clang-format on
}

std::string::size_type token::length() {
    if (data.has_value()) // clang-format off
        return std::visit(destinations{
            [](int& dig) { return std::to_string(dig).length(); },
            [](float& flt) { return std::to_string(flt).length(); },
            [](std::string& str) { return str.length(); },
            [](symbol sym) { return sym.raw.length(); },
            [](comment cmt) { return cmt.text.length(); }
        }, data.value());
    else return 1; // clang-format on
}

std::string kind_string(const token_kind& kindToCheck) {
    switch (kindToCheck) { // clang-format off
    #define t(k) case token_kind::k : return std::string(#k);
    KIND_LIST
    #undef t
    default: return std::string("Unknown");
    } // clang-format on
}

} // namespace qlc
