#include <algorithm>
#include <experimental/filesystem>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <string>
#include <string_view>
#include <vector>

#include "scanner.hpp"
#include "token.hpp"

namespace fs = std::experimental::filesystem;

const std::string usage = "usage: qlc [--version] [--help] [<filename>]";

int main(int argc, char* argv[]) {
    if (argc == 1) {
        std::cout << usage + "\n";
        return 0;
    }

    std::vector<std::string> args;
    for (auto i = 1; i < argc; ++i)
        args.push_back(std::string(argv[i]));

    if (auto v = std::find(args.begin(), args.end(), "--version"); v != args.end()) {
        std::cout << "Quarrel LLVM Compiler - v0.1.0\n";
        args.erase(v);
    }

    if (auto h = std::find(args.begin(), args.end(), "--help"); h != args.end()) {
        std::cout << usage + "\n";
        args.erase(h);
    }

    if (!args.empty()) {
        std::ifstream file;
        fs::path pathArg;
        for (auto iter = args.begin(); iter != args.end(); iter++) {
            if (std::string_view arg(*iter); arg.substr(0, 1) == "-")
                continue;
            else {
                if (pathArg = fs::path(arg); fs::exists(pathArg) && fs::is_regular_file(pathArg)) {
                    file.open(pathArg);
                    break;
                }
            }
        }
        qlc::scanner scanner(file, pathArg.filename());
        for (qlc::token current; scanner >> current;)
            std::cout << current.to_string() << std::endl;
        std::cout << "\n";
        if (file.bad())
            std::cout << "I/O error while reading\n";
        else if (file.eof())
            std::cout << "End of file reached successfully\n";
        else if (file.fail())
            std::cout << "Non-integer data encountered\n";
    }

    return 0;
}
