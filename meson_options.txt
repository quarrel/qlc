option('libcxx',
       type : 'boolean',
       value : false,
       description : 'Build with Clang\'s libc++ instead of libstdc++ on Linux')