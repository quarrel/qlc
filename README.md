# Quarrel Truffle Implementation

`qlc` is the Quarrel Language Compiler. It will be implemented in the Truffle
language framework and thus execute code in the GraalVM. Quarrel is in the final
review of design stage.

## What is Quarrel?

Quarrel is a high-level dynamically-typed functional programming language. It is
an extremely-unique beast-of-a-language that may or may not cause seizures in the
state of California.

The first version of the spec is right inside the base directory as `syntax.ql`,
if you are interested in perusing some of my design notes and seeing the current
"standard" Qaurrel specification. As features get added to the compiler, there
will surely be changes (hopefully none due to engineering challenges...). So be
mindful of this when setting any expectations.

Which reminds me, my ability to remain active in this project is fickle at best.
Please be prepared for periods of inactivity until I can secure a larger chunk
of time for the work involved.

## Don't we already have enough programming languages?

Quarrel is many things but first-and-foremost it is an idle brainchild. I designed
it around a challenge to create a language that had zero words for all builtin
functionality. All words in a Quarrel source file are user-generated. Obviously
there will surely be some form of standard library as there is little that you
can do to resolve things like `sin` and `cos`. A second-but-equal design
principle in Quarrel is to be entirely symbolized in generally-available symbols
that are present on modern keyboard formats. My curiosity was piqued as to whether
this would truly be possible: a non-word-based language with no special unicode
characters (though I did try to lean into ligatures). From there, one might
consider going the route of simplistic languages like Factor, Smalltalk, or Scheme
where the limited foundational functionality allows for an easier design process
and then places much of the typical standard functionality into libraries. Therein
lies my challenge, to build a comprehensive base language that provides as many
features as possible in symbols where significant scripts could be written without
ever having to dive into a library.

It brings me much joy to have identified (bruteforced?) an approach that provides
powerful primitives which can build complex programs without feeling the need to
call into hidden implementations behind a standard library routine. This aspect 
identified through the building process, became my tertiary goal: what kind of
primitives could I come up with that would be expressive, comprehensible and
symbol-only? If this has tickled your fancy, take a look but undertand that
Quarrel has bucked many common design elements in the hunt for a syntax that
feels natural (to me, mind) and is powerful. I had to swap symbols around to
ensure that there was a certain sense to the use of each symbol combination.
You may also find that the pattern sublanguage is additionally challenging, though
I did my best to avoid any crossover into the main language syntax. Finally, I
will note that there is likely going to be a need to have some overlap of operators
for different types and the semantics may be significantly different. At this time,
I do not feel that I've had to make many such compromises.

## How do I grok Quarrel?

You can start by knowing that Quarrel is dynamically-typed and functional. There
is a main syntax and a pattern language. Everything is symbols so getting familiar
with Quarrel takes time but I believe that our brains grok symbols with even
greater efficacy than words. I curated the symbol (and compound symbol) choices
to feel natural to the semantics.

### Patterns

Patterns provide the power that type systems bring to static languages and patterns
are also first-class primitives. Additionally, they are used to describe compound
and enumerable data.

### Dataflow

Many of Quarrel's data operations (such as transforms) can be performed in
left-to-right or right-to-left fashion. This provides a useful visual and semantic
guide to how the data is flowing through your code.

### Transforms

Quarrel implements looping and data operations into a unique operator called the
transform operator. The operator can determine what type of action to take from
what is on the left and right. There are several subtypes of the transform operator
that provide additional functionality.

## But...wat

Quarrel has lofty dreams of being a language that bucks the trend of
"anglophonocentrism" in programming. That meaning the fact that most languages
still utilize words like `for`, `if`, `is`, `while` and many more. Also, there are
ancient tropes that have stuck with programming from its heyday like `bool`, `del`,
`func`, `sub`, and `def`. These words (especially the ancient and abbreviated ones) provide an additional impedance to non-native english-speakers who want to learn programming. Unfortunately, I fear that I cannot completely eliminate the need for
named things like `integer`, `string`, `float`, and more (though I do plan on
changing these names to be less anglophonocentric).

If the language ever becomes fully implemented and keeping design considerations
in mind during implementation, I plan to tackle this through a language server
that translates all words between human languages. This will utilize some kind of
translation file where the library/app developer will have a file that contains
custom translations. When working with developers in different languages, there
will need to be some process by which developers who have selected a human langauge
that is not the original source language will be given a popup to add their own
translations.